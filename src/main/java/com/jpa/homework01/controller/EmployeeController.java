package com.jpa.homework01.controller;

import com.jpa.homework01.entity.Employee;
import com.jpa.homework01.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("api/v1/employee")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeRepository employeeRepository;



    @PostMapping("/persist")
    public ResponseEntity<?> persist(){
        Employee employee = new Employee(null, "Thyvidijame", "Me","jameleo7777@gmail.com", new Date(),null);
        employeeRepository.persist(employee);
        return ResponseEntity.ok().body(employee);
    }

    @PostMapping("/detach")
    public ResponseEntity<?> detach(){
        Employee employee = new Employee(null, "Socheeat", "Mann", "socheatmann2020@gmail.com", new Date(), null);
        employeeRepository.detach(employee);
        return ResponseEntity.ok().body(employee);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        Employee employee = employeeRepository.findById(id);
        return ResponseEntity.ok().body(employee);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> remove(@PathVariable("id") Long id){
        employeeRepository.remove(id);
        return ResponseEntity.ok().body("Successfully remove object has id : "+id);
    }
}
