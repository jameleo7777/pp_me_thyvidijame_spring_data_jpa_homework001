package com.jpa.homework01;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(
        info = @Info(
                title = "JPA/Hibernate-01",
                version = "3.14"
        )
)
@SpringBootApplication
public class Homework01Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework01Application.class, args);
    }

}
