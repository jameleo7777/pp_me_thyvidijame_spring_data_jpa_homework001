package com.jpa.homework01.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "employee_tb")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_id")
    private Long id;

    @Column(name = "first_name", length = 50)
    private String firstName;


    @Column(name = "last_name", length = 50)
    private String lastName;


    @Column(name = "email", length = 250 , unique = true)
    private String email ;


    @Temporal(TemporalType.DATE)
    private Date date;

    @Transient
    private String temp;
}
