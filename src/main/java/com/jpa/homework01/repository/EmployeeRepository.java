package com.jpa.homework01.repository;


import com.jpa.homework01.entity.Employee;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class EmployeeRepository {

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void persist(Employee employee){
        em.persist(employee);
    }


    @Transactional
    public void detach(Employee employee){
        em.detach(employee);
        employee.setFirstName("Diego");
        employee.setLastName("Dorana");
        employee.setEmail("jack@gmail.com");
        em.merge(employee);
        em.flush();
    }


    @Transactional
    public Employee findById(Long id){
        Employee employee = em.find(Employee.class, id);
        return employee;
    }

    @Transactional
    public void remove(Long id){
        Employee employee = em.find(Employee.class,id);
        em.remove(employee);
    }



}
